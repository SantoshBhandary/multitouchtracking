# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Creating a simple custom view which tracks the user fingers. Meaning as
the user places the finger in the screen, a circle is drawn.
* With Different fingers different circle is drawn (i.e multi touch).
* Once the user moves the finger those circle also follows the finger.
* Once the user removes the finger, the circle also goes off.
* Version 1
* [Learn Markdown](https://SantoshBhandary@bitbucket.org/SantoshBhandary/multitouchtracking.git)

### How do I get set up? ###

* To set up this project , First Blank Activity project is created
* A Custom class called "MultitouchView" is created for the user to where the Custom class extends "View" and overrides methods such as "onTouchEvent" and "onDraw".
* In custom view some predefined classes such as Canvas,Color,Paint,PointF,AttributeSet,SparseArray,MotionEvent and View are used inorder to get MultiTouch along with SwipeGesture
* Main xml layout should include MultitouchView tag along with its pakage route inorder to render Multitouch feature for user to experience Multiple touch.


* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact